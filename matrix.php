<?php
// Function to form  
// lower triangular matrix 
function lower($matrix, $row, $col) 
{ 
    $i; $j; 
    for ($i = 0; $i < $row; $i++) 
    { 
        for ($j = 0; $j < $col; $j++) 
        { 
            if ($i < $j) 
            { 
                echo "0" , " "; 
            } 
            else
            echo $matrix[$i][$j] , " "; 
        } 
        echo "</br>"; 
    } 
}

// Function to form 
// upper triangular marix 
function upper($matrix, $row, $col) 
{ 
    $i; $j; 
      
    for ($i = 0; $i < $row; $i++) 
    { 
        for ($j = 0; $j < $col; $j++) 
        { 
            if ($i > $j) 
            { 
                echo "0" , " "; 
            } 
            else
            echo $matrix[$i][$j] ," "; 
        } 
    echo "</br>"; 
    } 
} 

// Driver Code 
$matrix = array (array (1, 2, 3), 
                  array (4, 5, 6), 
                 array (7, 8, 9)); 
$row = 3; $col = 3; 
  
echo "Lower triangular matrix: </br>"; 
lower($matrix, $row, $col); 
  
echo "Upper triangular matrix: </br>"; 
upper($matrix, $row, $col); 
      
// This code is contributed by jit_t
?>